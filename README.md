# Storage

Projects Storage

## Projects

| branch             | parent module   |
|--------------------|-----------------|
| [DRML](https://github.com/Lmy0217/Storage/tree/DRML) | [master@Deep Residual Metric Learning](https://github.com/Lmy0217/DRML) |
| [DRML_CKT](https://github.com/Lmy0217/Storage/tree/DRML_CKT) | [ckt@Deep Residual Metric Learning](https://github.com/Lmy0217/DRML/tree/ckt) |
| [FullScale](https://github.com/Lmy0217/Storage/tree/FullScale) | [fullscale@Deep Residual Metric Learning](https://github.com/Lmy0217/DRML/tree/fullscale) |

## Citation
**Every branch citation is inherit from its parent module citation**.

## License
**Every branch license is inherit from its parent module license**.
